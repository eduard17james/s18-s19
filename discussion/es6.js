var city = 'London';
console.log(city);

//1. issues with var
//a. same variable name to be used over and over again.
 
var city = 'Tokyo';
console.log(city);

// let/const - new set of rules implemented to avoid sloppy coding

let name = 'Iron Man';
if (true){
	let name = 'Captain America';
};

console.log(name);

//2. update to the pow()
     // pow() - Math.pow(base, exponent), this function will allow you to return the base to the exponent power.

     console.log(Math.pow(7,2));
     console.log(Math.pow(7,3));

     console.log(7**3);
     console.log(7**2);


// Using template literals
// 1. `${variable name}`
let username = 'Martin';
console.log(`Hello my name is ${username},Please to meet you!`);

console.log(`
________00000000000___________000000000000_________
______00000000_____00000___000000_____0000000______
____0000000_____________000______________00000_____
___0000000_______________0_________________0000____
__000000____________________________________0000___
__00000_____________________________________ 0000__
_00000______________________________________00000__
_00000_____________________________________000000__
__000000_________________________________0000000___
___0000000______________________________0000000____
_____000000____________________________000000______
_______000000________________________000000________
__________00000_____________________0000___________
_____________0000_________________0000_____________
_______________0000_____________000________________
_________________000_________000___________________
_________________ __000_____00_____________________
______________________00__00_______________________
________________________00_________________________

	`);

// How to destructure arrays and objects

let fullname = ['Martin', 'Miguel', 'Manila'];

console.log(`
	My Last name is ${fullname[1]}
    My first name ${fullname[0]}
    I live in ${fullname[2]}
`)



let students = ['Maria', 'Mario', 'Luigi'];

let  [studentA, studentB, studentC] = students;

console.log(studentA, studentB, studentC);

let information = ['Jaimes','Barlizo', 'Puraran'];

const [firstname, lastname, location]=information;

console.log(firstname);
console.log(lastname);
console.log(location);

let users = ['Mario', 'Luigi', 'Peach', 'Bowser'];

let [char1, char2, char3, char4] = users;

console.log(char3);
console.log(char4);


const person = {
	givenName: 'Jaimes',
	maidenName: 'Dela',
	familyName: 'Cruz',
	homeAddress: 'Puraran'
};

console.log(person.homeAddress);
console.log(person.givenName);

// Object Destructuring

const {givenName, maidenName, familyName, homeAddress} = person;

console.log(givenName);
console.log(homeAddress);
console.log(familyName);


let userDetails = {
   	  uname: ['badboii4life', 'badboii4life2', 'simpleLifeLang'], 
   	  email: 'juan@email.com',
   	  password: 'password1234',
   	  gender: 'Male',
   	  scienceGrade: {
   	  	firstGrading: 89,
   	  	secondGrading: 88,
   	  	thirdGrading: 87, 
   	  	fourthGrading: 85
   	  } 
   }

   //destucture the object above. 
   let {email, password, gender, uname, scienceGrade } = userDetails;
   let { firstGrading, secondGrading, thirdGrading, fourthGrading} = scienceGrade;  
   let [ uName1, uName2, uName3 ] = uname;

   console.log(`The password is ${password}`); 
   console.log(uName3); //you can use the the index count of the element inside the array. 
   console.log(`The user is ${gender}`); 
   console.log(thirdGrading); 

// Declairing arrow based functions

const generateFullName = (fName, mName, lName) => {
	console.log(`${lName}, ${fName} ${mName}`);
}

generateFullName('John', 'Fitzgerald', 'Kennedy');




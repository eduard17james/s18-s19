// YOU CAN PRACTICE ON THE FOLLOWING EXAMPLES BELOW: 
//    Convert the given code using ARROW BASED FUNCTIONS IN ES6 updates.
       

       const getCube = (num) => {
          return num ** 3;
       };

       getCube(5);


       let numbers = [1, 2, 3, 4, 5];

       let [num1, num2, num3, num4, num5] = numbers;

       console.log(num1);

       let numberMap = numbers.map((number) => {
           return number * number;
        });

       console.log(numberMap);

       let allValid = numbers.every((number) => {
          return number < 3;
      });

       console.log(allValid);

       let someValid = numbers.some((number) => {
          return number < 2;
       });

       console.log(someValid);

       let filterValid = numbers.filter((number) => {
          return (number <  3);
       });

       console.log(filterValid);

       let reduceNumber = numbers.reduce((x, y) => {
          return x + y;
       });

       console.log(reduceNumber);
    
    
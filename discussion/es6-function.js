// Arrow based functions:

// Syntax () => *function expression*


const getSum = (num1, num2) => num1 + num2;
console.log(getSum(5,3));



const getProduct = (x, y) => {
	return x - y;
    return x + y;
    return x * y;
};
console.log(getProduct(2, 8));


// Class-Based Object Blueprints

class Car {
	constructor(brand, name, year){
		this.carName = name;
		this.carBrand= brand;
		this.Manufacturer = year;
	}
}


let car1 = new Car('Trueno (AE86', 'Toyota', '1984');
let car2 = new Car('Mitsubishi Fortuner', 'Mitsubishi', '2010');

let caCollection = [car1, car2];
console.table(caCollection);

// Default function Argument Values:

const greetings = (name) => console.log(`Welcome to the ${name}`)

greetings('paul');

